import styled from "styled-components";

export const Button = ({ content }) => {
  return <StyledButton>{content}</StyledButton>;
};

const StyledButton = styled.button`
  background: linear-gradient(to right, #a12ac4 0%, #ed586c 40%, #f0a853 100%);
  text-transform: uppercase;
  letter-spacing: 0.2rem;
  width: 65%;
  height: 3rem;
  border: none;
  color: white;
  border-radius: 2rem;
  cursor: pointer;
`;
