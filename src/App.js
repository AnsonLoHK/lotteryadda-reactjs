import "./App.scss";
import React from "react";
import Register from "./Register";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Register />
      </Router>
    </div>
  );
}

export default App;

/* <div>
          <a
            href="https://wa.me/+919096777398"
            target="_blank"
            rel="noreferrer"
          >
            <i className="icon fab fa-whatsapp"></i>
          </a>
          <a href="https://t.me/+919096777398" target="_blank" rel="noreferrer">
            <i className="icon fab fa-telegram"></i>
          </a>
        </div> */
