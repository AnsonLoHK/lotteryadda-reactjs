import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useNotification } from "./Notifiction/NotificationProvider";
import { useLocation } from "react-router-dom";
import "./styles.scss";
import { FaExclamationCircle, FaCheckCircle } from "react-icons/fa";
import RegisterForm from "./components/Phone/RegisterForm";
import "../src/components/mobile/mobileModeRegist.scss";
import {
  faCircleCheck,
  faCircleXmark,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import download from "./assets/images/download.png";
// http://api.testing.ganeshaer.in 原本
const REGISTER_URL = "https://api.game.lotteryadda.com/sso/registersForOut";
// 匹配 英文字母開頭 / Letters, numbers, underscores, hyphens 的內容 / 且4~24位數 則符合匹配
const USER_REGEX = /^[a-z][a-z0-9]{5,5}$/;
// 簡訊碼OTP
const SMS_REGEX = /^[0-9][0-9]{5,5}$/;

const Register = () => {
  const userRef = useRef();
  const [screenSize, getDimension] = useState({
    dynamicWidth: window.innerWidth,
    dynamicHeight: window.innerHeight,
  });
  const [userFocus, setUserFocus] = useState(false);
  const [code, setCode] = useState("");
  const [sms, setSms] = useState("");
  const [validName, setValidName] = useState(false);
  const [validSms, setValidSms] = useState(false);
  const [regist, setRegist] = useState("");
  const [mobile, setMobile] = useState(false);
  const [successForInviteCode, setSuccessForInviteCode] = useState(null);
  const [successForCheckSms, setSuccessForCheckSms] = useState(null);
  const [shareState, setShareState] = useState();

  let query = useQuery();
  const dispatch = useNotification();

  const setDimension = () => {
    getDimension({
      dynamicWidth: window.innerWidth,
      dynamicHeight: window.innerHeight,
    });
  };

  useEffect(() => {
    setValidName(USER_REGEX.test(code));
  }, [code]);

  useEffect(() => {
    setValidSms(SMS_REGEX.test(sms));
  }, [sms]);

  useEffect(() => {
    const codeVlaue = query.get("inviteCode");
    setValidName(USER_REGEX.test(code));
    setCode(codeVlaue);
  }, []);

  useEffect(() => {
    console.log("123", shareState);
  }, [shareState]);

  /* 重新渲染 區 */

  // 螢幕大小偵測
  useEffect(() => {
    window.addEventListener("resize", setDimension);

    if (screenSize.dynamicWidth <= 800) {
      setMobile(true);
    } else {
      setMobile(false);
    }

    return () => {
      window.removeEventListener("resize", setDimension);
    };
  }, [screenSize, mobile]);

  /* input區變化區 */

  const changeinviteCode = (e) => {
    const value = e.target.value;
    setCode(value);
    checkInviteCode(value);
  };

  const onChangeSms = (e) => {
    const value = e.target.value;
    checkSms(value);
    setSms(value);
  };

  /*表單送出 */
  const handleSubmit = async (e) => {
    e.preventDefault();

    if (validName && validSms) {
      dispatch({
        type: "SUCCESS",
        message: "Successful Request",
        title: "Successful Request",
      });
    } else {
      dispatch({
        type: "ERROR",
        message: "Invalid Entry",
        title: "Un Successful Request",
      });
    }

    const phone = shareState;
    const captcha = sms;
    const inviteCode = code;
    console.log("phone", phone);
    var config = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
      },
      responseType: "json", // default
      method: "post",
    };
    axios
      .post(
        REGISTER_URL,
        {
          phone: phone,
          captcha: captcha,
          inviteCode: inviteCode,
        },
        config
      )
      .then((res) => {
        if (res) {
          setRegist("please click this url to download the app:");
        }
      })
      .catch((err) => {
        dispatch({
          type: "ERROR",
          message: err,
          title: "Un Successful Request",
        });
      });
  };

  function checkInviteCode(value) {
    const checkInviteCode = value;
    if (checkInviteCode === "") {
      return;
    } else if (checkInviteCode.length === 10) {
      setSuccessForInviteCode(true);
    } else {
      setSuccessForInviteCode(false);
    }
  }

  function checkSms(value) {
    const checkSms = value;
    if (checkSms.length === 6) {
      setSuccessForCheckSms(true);
    } else {
      setSuccessForCheckSms(false);
    }
  }

  return (
    <>
      {!mobile ? (
        <div className="regist-bgimg-container">
          <div className="_3M9lzn PeA8Gc regist-bgimg">
            <form className="login form" onSubmit={handleSubmit}>
              <div className="loginContainer color flex">
                {/* 邀請碼 */}
                <div
                  className={`form-control ${
                    successForInviteCode ? "success" : "error"
                  }`}
                >
                  <label htmlFor="phoneNumber">
                    nviteCode:
                    <FontAwesomeIcon
                      icon={faCircleCheck}
                      //   驗證成功=>顯示勾勾
                      className={validName ? "valid" : "hide"}
                    />
                    <FontAwesomeIcon
                      icon={faCircleXmark}
                      //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
                      className={validName || !code ? "hide" : "invalid"}
                    />
                  </label>
                  <input
                    className="input-box"
                    type="text"
                    value={code}
                    ref={userRef}
                    autoComplete="off"
                    onChange={changeinviteCode}
                    aria-invalid={validName ? "false" : "true"}
                    aria-describedby="uidnote"
                    onFocus={() => setUserFocus(true)}
                    placeholder="inviteCode.."
                  />
                  <p
                    id="uidnote"
                    //   點選input框+開始輸入內容+驗證失敗
                    //   輸入內容非4~2
                    className={
                      userFocus && code && !validName
                        ? "instructions"
                        : "offscreen"
                    }
                  >
                    InvideCode is invalid, Must have 6 alphabet
                  </p>
                </div>
                {/* 手機碼 */}
                <div
                  className={`form-control ${
                    successForInviteCode ? "success" : "error"
                  }`}
                >
                  <RegisterForm setShareState={setShareState} />
                </div>
                {/* 驗證碼 */}
                <div
                  className={`form-control ${
                    successForInviteCode ? "success" : "error"
                  }`}
                >
                  <label htmlFor="phoneNumber">
                    OTP:
                    <FontAwesomeIcon
                      icon={faCircleCheck}
                      //   驗證成功=>顯示勾勾
                      className={validSms ? "valid" : "hide"}
                    />
                    <FontAwesomeIcon
                      icon={faCircleXmark}
                      //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
                      className={validSms || !sms ? "hide" : "invalid"}
                    />
                  </label>
                  <input
                    className="input-box"
                    type="text"
                    value={sms}
                    ref={userRef}
                    autoComplete="off"
                    onChange={onChangeSms}
                    aria-invalid={validSms ? "false" : "true"}
                    aria-describedby="uidnote"
                    onFocus={() => setUserFocus(true)}
                    placeholder="OTP.."
                  />
                  <p
                    id="uidnote"
                    //   點選input框+開始輸入內容+驗證失敗
                    //   輸入內容非4~2
                    className={
                      userFocus && sms && !validSms
                        ? "instructions"
                        : "offscreen"
                    }
                  >
                    InvideCode is invalid, Must have 6 alphabet
                  </p>
                </div>
                {/* 註冊按鈕 */}
                <button className="defaultBytton">Regist</button>
                <span className="download">
                  {regist ? (
                    <a
                      href="https://google.com"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <label>Press the download icon</label>
                      <br />
                      <img src={download} alt="example" />
                    </a>
                  ) : (
                    <></>
                  )}
                </span>
              </div>
            </form>
          </div>
        </div>
      ) : (
        <>
          <div>
            <div className="container margin-top">
              <div className="card">
                <div className="inner-box">
                  <div className="card-back">
                    <form className="" onSubmit={handleSubmit}>
                      <div className="mobile-form">
                        {/* 邀請碼 */}
                        <div
                          className={`form-control ${
                            successForInviteCode ? "success" : "error"
                          }`}
                        >
                          <label>inviteCode</label>
                          <input
                            className="input-box"
                            type="text"
                            value={code}
                            onChange={changeinviteCode}
                            placeholder="inviteCode.."
                          />
                          {successForInviteCode ? (
                            <FaCheckCircle
                              className="icon success"
                              size="20px"
                            />
                          ) : (
                            <FaExclamationCircle
                              className="icon error"
                              size="20px"
                            />
                          )}
                          {successForInviteCode ? (
                            <small className="green">Valid InviteCode</small>
                          ) : (
                            <small className="red">
                              Not a valid InviteCode
                            </small>
                          )}
                        </div>
                        {/* 手機碼 */}
                        <div
                          className={`form-control ${
                            successForInviteCode ? "success" : "error"
                          }`}
                        >
                          <RegisterForm setShareState={setShareState} />
                        </div>
                        {/* 驗證碼 */}
                        <div
                          className={`form-control  ${
                            successForCheckSms ? "success" : "error"
                          }`}
                        >
                          <input
                            className="input-box"
                            type="text"
                            value={sms}
                            onChange={onChangeSms}
                            placeholder="Enter captcha 888888"
                          />
                          {successForCheckSms ? (
                            <FaCheckCircle
                              className="icon success CheckSms"
                              size="20px"
                            />
                          ) : (
                            <FaExclamationCircle
                              className="icon error CheckSms"
                              size="20px"
                            />
                          )}

                          {successForCheckSms ? (
                            <small className="green">Valid Captcha</small>
                          ) : (
                            <small className="red">
                              Captcha cannot be blank
                            </small>
                          )}
                        </div>
                        {/* 註冊按鈕 */}
                        <button className="defaultBytton">Regist</button>
                        <span className="download">
                          {regist ? (
                            <a href="https://play.google.com/store/apps/details?id=com.kitkagames.fallbuddies&gl=US">
                              Now Please Download the App
                            </a>
                          ) : (
                            "After regist the phone number you will get link"
                          )}
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="container ad-photo">
              <div className="card">
                <div className="inner-box">
                  <div className="card-back-ad" />
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Register;

// A custom hook that builds on useLocation to parse
// the query string for you.
function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}
