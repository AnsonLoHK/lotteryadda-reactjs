import React from "react";
import "./DropdownMenu.scss";
import useStyles from "./Subheader.styles";
import MyMaterial from "../myMaterial/MyMaterial";

const DropdownMenu = () => {
  const classes = useStyles();
  const [age, setAge] = React.useState("91");
  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const { subheaderContainer } = classes;

  return (
    <div className="phone-container">
      <MyMaterial.FormControl sx={{ m: 1, minWidth: 120 }}>
        <MyMaterial.Select
          className={subheaderContainer}
          labelId="demo-simple-select-readonly-label"
          id="demo-simple-select-readonly"
          value={age}
          label="India"
          onChange={handleChange}
          inputProps={{ readOnly: false }}
          renderValue={(value) => `+${value}`}
        >
          <MyMaterial.MenuItem value={"91"}>+91</MyMaterial.MenuItem>
          {/* <MyMaterial.MenuItem value={20}>Twenty</MyMaterial.MenuItem>
        <MyMaterial.MenuItem value={30}>Thirty</MyMaterial.MenuItem> */}
        </MyMaterial.Select>
      </MyMaterial.FormControl>
    </div>
  );
};

export default DropdownMenu;
