import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  subheaderContainer: {
    // background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    // boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "#000",
    borderRadius: "20px 0 0 20px",

    // marginRight: "3px",
  },
});

export default useStyles;
