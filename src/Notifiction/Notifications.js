import React, { useState, useEffect, useRef } from "react";

// The notification component just show the props and handle a
// "progress bar" for the notification changing the width.
const Notifications = (props) => {
  const [exit, setExit] = useState(false);
  const intvl = useRef(null);
  const [count, setCount] = useState(0);
  const [pause, setPause] = useState(false);
  const handleStartTimer = () => setPause(false);
  const handlePauseTimer = () => setPause(true);

  const handleCloseNotification = () => {
    handlePauseTimer();
    setExit(true);
    setTimeout(() => {
      props.dispatch({
        type: "REMOVE_NOTIFICATION",
        id: props.id,
      });
    }, 400);
  };
  const inc = () => setCount((prev) => prev + 5);

  useEffect(() => {
    !pause
      ? (intvl.current = setInterval(inc, 100))
      : clearInterval(intvl.current);

    count === 100 && clearInterval(intvl.current);
  }, [pause, count]);

  useEffect(() => {
    if (count === 100) {
      // Close notification
      handleCloseNotification();
    }
  }, [count]);

  useEffect(() => {
    handleStartTimer();
  }, []);

  return (
    <div
      onMouseEnter={handlePauseTimer}
      onMouseLeave={handleStartTimer}
      className={`notification-item ${
        props.type === "SUCCESS" ? "success" : "error"
      } ${exit ? "exit" : ""}`}
    >
      <p>{props.message}</p>
      <div className={"bar"} style={{ width: `${count}%` }}></div>
    </div>
  );
};

export default Notifications;
