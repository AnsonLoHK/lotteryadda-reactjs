import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useNotification } from "./Notifiction/NotificationProvider";
import { useLocation } from "react-router-dom";
import RegisterForm from "./components/Phone/RegisterForm";
import "./styles.scss";
import "../src/components/mobile/mobileModeRegist.scss";
import {
  faCircleCheck,
  faCircleXmark,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import downloadChrome from "./assets/images/download.png";
import downloadFireFox from "./assets/images/download-Firefox.png";
import Background from "./assets/images/Background.png";
import BackgroundMobile from "./assets/images/mobile.png";
import BackgroundMobiles from "./assets/images/Frame1318.png";

import { BrowserView, browserName, CustomView } from "react-device-detect";
// http://api.testing.ganeshaer.in/sso/registersForOut
const REGISTER_URL = "https://api.game.lotteryadda.com/sso/registersForOut";
// 匹配 英文字母開頭 / Letters, numbers, underscores, hyphens 的內容 / 且4~24位數 則符合匹配
const USER_REGEX = /^[a-z][a-z0-9]{5,5}$/;
// 簡訊碼OTP
const SMS_REGEX = /^[0-9][0-9]{5,5}$/;

const Register = () => {
  const userRef = useRef();
  const [screenSize, getDimension] = useState({
    dynamicWidth: window.innerWidth,
    dynamicHeight: window.innerHeight,
  });
  const [userFocus, setUserFocus] = useState(false);
  const [code, setCode] = useState("");
  const [sms, setSms] = useState("");
  const [validName, setValidName] = useState(false);
  const [validSms, setValidSms] = useState(false);
  const [regist, setRegist] = useState("");
  const [mobile, setMobile] = useState(false);
  const [successForInviteCode, setSuccessForInviteCode] = useState(null);
  const [successForCheckSms, setSuccessForCheckSms] = useState(null);
  const [shareState, setShareState] = useState();

  const Chrome = {
    marginTop: "12px",
    width: "64%",
    height: "30%",
    fontSize: "24px",
    lineHeight: "2",
  };

  const Firefox = {
    marginTop: "12px",
    width: "64%",
    height: "30%",
    fontSize: "24px",
    lineHeight: "2",
  };
  let query = useQuery();
  const dispatch = useNotification();

  const setDimension = () => {
    getDimension({
      dynamicWidth: window.innerWidth,
      dynamicHeight: window.innerHeight,
    });
  };

  useEffect(() => {
    setValidName(USER_REGEX.test(code));
  }, [code]);

  useEffect(() => {
    setValidSms(SMS_REGEX.test(sms));
  }, [sms]);

  useEffect(() => {
    const codeVlaue = query.get("inviteCode");
    setValidName(USER_REGEX.test(code));
    setCode(codeVlaue);
  }, []);

  useEffect(() => {
    console.log("This is your phone number", shareState);
  }, [shareState]);

  /* 重新渲染 區 */

  // 螢幕大小偵測
  useEffect(() => {
    window.addEventListener("resize", setDimension);

    if (screenSize.dynamicWidth <= 600) {
      setMobile(true);
    } else {
      setMobile(false);
    }

    return () => {
      window.removeEventListener("resize", setDimension);
    };
  }, [screenSize, mobile]);

  /* input區變化區 */

  const changeinviteCode = (e) => {
    const value = e.target.value;
    setCode(value);
    checkInviteCode(value);
  };

  const onChangeSms = (e) => {
    const value = e.target.value;
    checkSms(value);
    setSms(value);
  };

  /*表單送出 */
  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("觸發");

    const phone = shareState;
    const captcha = sms;
    const inviteCode = code;
    console.log("phone", phone);
    var config = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
      },
      responseType: "json", // default
      method: "post",
    };
    axios
      .post(
        REGISTER_URL,
        {
          phone: phone,
          captcha: captcha,
          inviteCode: inviteCode,
        },
        config
      )
      .then((res) => {
        let resCode = res.data.code;
        console.log(resCode);
        if (res && resCode === 200) {
          dispatch({
            type: "SUCCESS",
            message: "Successful Request",
            title: "Successful Request",
          });
          setRegist("please click this url to download the app:");
        } else {
          dispatch({
            type: "ERROR",
            message: "OTP is incorrect",
            title: "OTP is incorrect",
          });
        }
      })
      .catch((err) => {
        dispatch({
          type: "ERROR",
          message: err,
          title: "Un Successful Request",
        });
      });
  };

  function checkInviteCode(value) {
    const checkInviteCode = value;
    if (checkInviteCode === "") {
      return;
    } else if (checkInviteCode.length === 10) {
      setSuccessForInviteCode(true);
    } else {
      setSuccessForInviteCode(false);
    }
  }

  function checkSms(value) {
    const checkSms = value;
    if (checkSms.length === 6) {
      setSuccessForCheckSms(true);
    } else {
      setSuccessForCheckSms(false);
    }
  }

  return (
    <>
      <div className="desktop">
        <div
          className="_3M9lzn PeA8Gc banner-area"
          style={{ backgroundImage: "url(" + Background + ")" }}
        >
          <div class="content-area">
            <form className="login form" onSubmit={handleSubmit}>
              <div className="loginContainer color flex">
                {/* 邀請碼 */}
                <div
                  className={`form-control ${
                    successForInviteCode ? "success" : "error"
                  }`}
                >
                  <label htmlFor="phoneNumber">
                    InviteCode:
                    <FontAwesomeIcon
                      icon={faCircleCheck}
                      //   驗證成功=>顯示勾勾
                      className={validName ? "valid" : "hide"}
                    />
                    <FontAwesomeIcon
                      icon={faCircleXmark}
                      //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
                      className={validName || !code ? "hide" : "invalid"}
                    />
                  </label>
                  <input
                    type="text"
                    className="input-box"
                    value={code}
                    ref={userRef}
                    autoComplete="off"
                    onChange={changeinviteCode}
                    aria-invalid={validName ? "false" : "true"}
                    aria-describedby="uidnote"
                    onFocus={() => setUserFocus(true)}
                    placeholder="InviteCode.."
                  />
                  <p
                    id="uidnote"
                    //   點選input框+開始輸入內容+驗證失敗
                    //   輸入內容非4~2
                    className={
                      userFocus && code && !validName
                        ? "instructions"
                        : "offscreen"
                    }
                  >
                    InvideCode is invalid, Must have 6 alphabet
                  </p>
                </div>
                {/* 手機碼 */}
                <div
                  className={`form-control ${
                    successForInviteCode ? "success" : "error"
                  }`}
                >
                  <RegisterForm setShareState={setShareState} />
                </div>

                {/* 驗證碼 */}
                <div
                  className={`form-control ${
                    successForInviteCode ? "success" : "error"
                  }`}
                >
                  <label htmlFor="phoneNumber">
                    OTP:
                    <FontAwesomeIcon
                      icon={faCircleCheck}
                      //   驗證成功=>顯示勾勾
                      className={validSms ? "valid" : "hide"}
                    />
                    <FontAwesomeIcon
                      icon={faCircleXmark}
                      //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
                      className={validSms || !sms ? "hide" : "invalid"}
                    />
                  </label>
                  <input
                    className="input-box"
                    required
                    type="text"
                    value={sms}
                    ref={userRef}
                    autoComplete="off"
                    onChange={onChangeSms}
                    aria-invalid={validSms ? "false" : "true"}
                    aria-describedby="uidnote"
                    onFocus={() => setUserFocus(true)}
                    placeholder="OTP.."
                  />
                  <p
                    id="uidnote"
                    //   點選input框+開始輸入內容+驗證失敗
                    //   輸入內容非4~2
                    className={
                      userFocus && sms && !validSms
                        ? "instructions"
                        : "offscreen"
                    }
                  >
                    The OTP numbers must have 6 digits
                  </p>
                </div>
                {/* 註冊按鈕 */}
                <button className="defaultBytton">Regist</button>
                <span className="download-desktop">
                  {regist ? (
                    <a
                      href="https://dl-game.lotteryadda.com/lotteryadda-latest.apk"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <div className="center-desktop-desktop">
                        <label className="">Press the download icon</label>
                        {browserName === "Chrome" ? (
                          <CustomView condition={browserName === "Chrome"}>
                            <BrowserView style={Chrome}>
                              <img
                                className="download-icon-desktop"
                                src={downloadChrome}
                                alt="downloadChrome"
                              />
                            </BrowserView>
                          </CustomView>
                        ) : (
                          <CustomView condition={browserName === "Firefox"}>
                            <BrowserView style={Firefox}>
                              <img
                                className="download-icon-desktop"
                                src={downloadFireFox}
                                alt="downloadFireFox"
                              />
                            </BrowserView>
                          </CustomView>
                        )}
                        {/* <img
                          className="download-icon-desktop"
                          src={download}
                          alt="example"
                        /> */}
                      </div>
                    </a>
                  ) : (
                    <></>
                  )}
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div>
        <div
          className="container-img-test"
          style={{
            backgroundImage: "url(" + BackgroundMobile + ")",
          }}
        >
          <form className="card" onSubmit={handleSubmit}>
            {/* 邀請碼 */}
            <div
              className={`form-control ${
                successForInviteCode ? "success" : "error"
              }`}
            >
              <label htmlFor="phoneNumber">
                InviteCode:
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  //   驗證成功=>顯示勾勾
                  className={validName ? "valid" : "hide"}
                />
                <FontAwesomeIcon
                  icon={faCircleXmark}
                  //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
                  className={validName || !code ? "hide" : "invalid"}
                />
              </label>
              <input
                className="input-box"
                type="text"
                value={code}
                ref={userRef}
                autoComplete="off"
                onChange={changeinviteCode}
                aria-invalid={validName ? "false" : "true"}
                aria-describedby="uidnote"
                onFocus={() => setUserFocus(true)}
                placeholder="InviteCode.."
              />
              <p
                id="uidnote"
                //   點選input框+開始輸入內容+驗證失敗
                //   輸入內容非4~2
                className={
                  userFocus && code && !validName ? "instructions" : "offscreen"
                }
              >
                InvideCode is invalid, Must have 6 alphabet
              </p>
            </div>
            {/* 手機碼 */}
            <RegisterForm setShareState={setShareState} />
            <div
              className={`form-control ${
                successForInviteCode ? "success" : "error"
              }`}
            ></div>
            {/* 驗證碼 */}
            <div
              className={`form-control  ${
                successForCheckSms ? "success" : "error"
              }`}
            >
              <label htmlFor="phoneNumber">
                OTP:
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  //   驗證成功=>顯示勾勾
                  className={validSms ? "valid" : "hide"}
                />
                <FontAwesomeIcon
                  icon={faCircleXmark}
                  //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
                  className={validSms || !sms ? "hide" : "invalid"}
                />
              </label>
              <input
                className="input-box"
                required
                type="text"
                value={sms}
                ref={userRef}
                autoComplete="off"
                onChange={onChangeSms}
                aria-invalid={validSms ? "false" : "true"}
                aria-describedby="uidnote"
                onFocus={() => setUserFocus(true)}
                placeholder="please enter your sms code"
              />
              <p
                id="uidnote"
                //   點選input框+開始輸入內容+驗證失敗
                //   輸入內容非4~2
                className={
                  userFocus && sms && !validSms ? "instructions" : "offscreen"
                }
              >
                OPT must have 6 digits.
              </p>
            </div>
            {/* 註冊按鈕 */}
            <button className="defaultBytton-regist">Regist</button>
            <span className="download">
              {regist ? (
                <a
                  href="https://dl-game.lotteryadda.com/lotteryadda-latest.apk"
                  target="_blank"
                  rel="noreferrer"
                >
                  <div className="center">
                    <label className="">Press the download icon</label>
                    <img
                      className="download-icon"
                      src={downloadChrome}
                      alt="example"
                    />
                  </div>
                </a>
              ) : (
                <></>
              )}
            </span>
          </form>
        </div>
      </div>

      <div
        className="container-img-test"
        style={{
          backgroundImage: "url(" + BackgroundMobiles + ")",
        }}
      ></div>
    </>
  );
};

export default Register;

// A custom hook that builds on useLocation to parse
// the query string for you.
function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}
