import React from "react";
import "./style.scss";

const MobileRegist = () => {
  return (
    <div class="container">
      <div class="card">
        <div class="inner-box">
          <div class="card-font">
            <form className="login form" onSubmit={handleSubmit}>
              <div className="loginContainer color">
                <label>InviteCode</label>
                <input
                  type="text"
                  value={code}
                  onChange={changeinviteCode}
                  placeholder="InviteCode.."
                />
                <label>Phone Number</label>
                <input
                  type="text"
                  value={tel}
                  onChange={ChangeInput}
                  placeholder="Enter your Phone Numbers..."
                />
                <button
                  className="defaultBytton"
                  disabled={num !== 0}
                  onClick={handleSend}
                >
                  {num === 0 ? "Send" : num + "s..."}
                </button>
                <input
                  type="text"
                  value={sms}
                  onChange={onChangeSms}
                  placeholder="Enter captcha 888888"
                />
                <button className="defaultBytton">Regist</button>

                <span className="download">
                  {regist ? (
                    <a href="https://play.google.com/store/apps/details?id=com.kitkagames.fallbuddies&gl=US">
                      Download
                    </a>
                  ) : (
                    "After regist the phone number you will get link"
                  )}
                </span>
              </div>
            </form>
          </div>
          <div class="card-back">
            <h2>Login</h2>
            <form>
              <input
                type="email"
                class="input-box"
                placeholder="your email id"
                required
              />
              <input
                type="email"
                class="input-box"
                placeholder="your email id"
                required
              />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MobileRegist;
