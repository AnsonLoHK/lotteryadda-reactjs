import React, { ChangeEvent, useEffect, useState } from "react";

function Effect() {
  let [num, setNum] = useState(0);
  let [tel, setTel] = useState("");

  const handleSend = () => {
    let a = 10;
    console.log(tel, "手机号");
    var reg_tel =
      /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
    if (reg_tel.test(tel)) {
      setNum(a);
      const t1 = setInterval(() => {
        a = a - 1;
        setNum(a);
        if (a == 0) {
          clearInterval(t1);
        }
      }, 1000);
    } else {
      alert("手机号格式不正确");
    }
  };
  //
  const onChangeInput = (e) => {
    console.log("Input改变", e.target.value);

    var reg_num = /^[0-9]*$/;
    if (reg_num.test(e.target.value)) {
      setTel(e.target.value);
      console.log(333);
    }
  };
  useEffect(() => {
    console.log("数据发生了变化,触发useEffect", num);
  });
  return (
    <div>
      <input type="text" value={tel} onChange={onChangeInput} />
      <button disabled={num !== 0} onClick={handleSend}>
        {num == 0 ? "发送" : num + "秒"}
      </button>
    </div>
  );
}

export default Effect;
