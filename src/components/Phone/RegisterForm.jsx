import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleCheck,
  faCircleXmark,
} from "@fortawesome/free-solid-svg-icons";
import "./RegisterForm.scss";
import DropdownMenu from "../../dropdownmenu/DropdownMenu";

// 匹配 英文字母開頭 / Letters, numbers, underscores, hyphens 的內容 / 且4~24位數 則符合匹配
// const USER_REGEX = /^[+A-z][A-z0-9-_]{3,23}$/;

const USER_REGEX = /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;

const RegisterForm = ({ setShareState }) => {
  const userRef = useRef();
  const [num, setNum] = useState(0);
  const [infoCapture, setInfoCapture] = useState({
    code: null,
    data: { exist: null, id: null },
    message: null,
  });
  const [user, setUser] = useState("");
  const [info, setInfo] = useState(false);
  const [validName, setValidName] = useState(false);
  const [userFocus, setUserFocus] = useState(false);

  useEffect(() => {
    setValidName(USER_REGEX.test(user));
  }, [user]);

  //  Test
  const handleSendTest = () => {
    let a = 10;
    // http://api.testing.ganeshaer.in/sso/getAuthCodesForOut?
    // 手機號碼post出去後 確認後台該手機是否存在
    const url = "https://api.game.lotteryadda.com/sso/getAuthCodesForOut?";

    axios
      .post(url, null, {
        headers: { "Content-Type": "application/json; charset=UTF-8" },
        params: { phone: user },
      })
      .then((response) => {
        const info = response.data;
        console.log("info", info.data.exist);
        if (info.data.exist === false) {
          setValidName(true);
        } else {
          setInfo(true);
          setValidName(false);
        }
        setInfoCapture(false);
      })
      .catch((err) => {
        console.log(err);
      });

    var reg_tel = /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;
    if (reg_tel.test(user)) {
      setNum(a);
      const t1 = setInterval(() => {
        a = a - 1;
        setNum(a);
        if (a === 0) {
          clearInterval(t1);
        }
      }, 1000);
    } else {
    }
  };

  const onChangePhone = (e) => {
    setUser(e.target.value);
    // 把phone number的值傳給 regist component
    setShareState(e.target.value);
  };

  return (
    <div>
      <form className="form-container">
        <label htmlFor="phoneNumber">
          PhoneNumber:
          <FontAwesomeIcon
            icon={faCircleCheck}
            //   驗證成功=>顯示勾勾
            className={validName ? "valid" : "hide"}
          />
          <FontAwesomeIcon
            icon={faCircleXmark}
            //   驗證成功=>隱藏X / 尚未輸入時 隱藏X
            className={validName || !user ? "hide" : "invalid"}
          />
        </label>
        <DropdownMenu />
        <input
          className="input-box"
          type="phone"
          maxLength="10"
          id="phoneNumber"
          ref={userRef}
          autoComplete="off"
          onChange={(e) => onChangePhone(e)}
          value={user}
          required
          aria-invalid={validName ? "false" : "true"}
          aria-describedby="uidnote"
          onFocus={() => setUserFocus(true)}
          placeholder="Enter your Phone Numbers..."
          //   onBlur={() => setUserFocus(false)}
        />

        {info ? (
          <p
            id="uidnote"
            //   點選input框+開始輸入內容+驗證失敗
            //   輸入內容非4~2
            className={
              userFocus && user && !validName ? "instructions" : "offscreen"
            }
          >
            The phone number already exist. You should try it later.
          </p>
        ) : (
          <p
            id="uidnote"
            //   點選input框+開始輸入內容+驗證失敗
            //   輸入內容非4~2
            className={
              userFocus && user && !validName ? "instructions" : "offscreen"
            }
          >
            Indian phone is invalid
            <br />
            Must begin with 91 or +91 .
            <br />
            acceptable format would be: +91 9456211568, 9775876662, 918880344456
          </p>
        )}

        <button
          className="defaultBytton"
          disabled={num !== 0}
          onClick={handleSendTest}
        >
          {num === 0 ? "send" : num + "s..."}
        </button>
      </form>
    </div>
  );
};

export default RegisterForm;
