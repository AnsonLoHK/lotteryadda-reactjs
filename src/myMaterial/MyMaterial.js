import { Select, MenuItem, InputLabel, FormControl } from "@material-ui/core";

export default {
  Select,
  MenuItem,
  InputLabel,
  FormControl,
};
