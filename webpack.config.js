//引用path模組
const path = require("path");
// const HtmlWebPackPlugin = require("html-webpack-plugin");
module.exports = {
  //這個webpack打包的對象，這裡面加上剛剛建立的index.js
  entry: __dirname + "/src/index.js", // 入口文件
  output: {
    //這裡是打包後的檔案名稱
    filename: "index.bundle.js",
    path: path.join(__dirname, "public"),
  },
  module: {
    rules: [
      // 创建模块时，匹配请求的规则数组。这些规则能够修改模块的创建方式。
      // 这些规则能够对模块(module)应用 loader，或者修改解析器(parser)
      {
        // 哪些模組要使用: test 判斷哪些模組適用此規則 解析.jsx结尾的模块(文件)
        test: /\.(js|jsx)$/,
        // 要做哪些處理: use 設定這些模組要用哪些 Loaders 做處理。
        use: [
          {
            loader: "babel-loader",
            // options: {
            //   presets: ["@babel/preset-env", "@babel/preset-react"],
            // }, }],
          },
        ],
        //排除node_modules下的js文件
        exclude: /(node_modules|bower_components)/,
        //要在 src 目錄中的 js文件
        // include: path.resolve(__dirname, "src"),
      },
      {
        test: /\.s[ac]ss$/i, // or /\.scss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: "url-loader",
        },
      },
    ],
  },
  resolve: {
    extensions: [".js", ".json", ".jsx", ".scss"],
  },
  devServer: {
    port: 3000,
    historyApiFallback: true,
    static: {
      directory: path.join(__dirname, "/public"),
    },
    open: true,
    hot: true,
  },
  mode: "none",
  // plugins: [new HtmlWebPackPlugin()], //用不到
};

/*備註*/
// 1.test 與 include 屬性的功能都是只要條件為真，此規則就會匹配。
// 2.exclude 屬性只要條件為真，此規則就會排除。
// 3.include: [path.resolve(__dirname, "src"),(path) => path.match(/app/)] 要在 src 目錄中 或 目錄中有符合 app 字串的檔案
// 4. exclude: path.resolve(__dirname, "app", "exclude"), exclude: 排除在 app/exclude 目錄下的檔案
// module: {
//     rules: [
//       // 创建模块时，匹配请求的规则数组。这些规则能够修改模块的创建方式。
//       // 这些规则能够对模块(module)应用 loader，或者修改解析器(parser)
//       {
//         // 哪些模組要使用: test 判斷哪些模組適用此規則 解析.jsx结尾的模块(文件)
//         test: /\.(js|jsx)$/,
//         // 要做哪些處理: use 設定這些模組要用哪些 Loaders 做處理。
//         use: [
//           {
//             loader: "babel-loader",
//             // options: {
//             //   presets: ["@babel/preset-env", "@babel/preset-react"],
//             // }, }],
//           },
//         ],
//         //排除node_modules下的js文件
//         exclude: /(node_modules|bower_components)/,
//         //要在 src 目錄中的 js文件
//         // include: path.resolve(__dirname, "src"),
//       },
//       {
//         test: /\.s[ac]ss$/i, // or /\.scss$/i,
//         use: [
//           // [style-loader](/loaders/style-loader)
//           { loader: "style-loader" },
//           // [css-loader](/loaders/css-loader)
//           {
//             loader: "css-loader",
//             options: {
//               modules: true,
//             },
//           },
//           // [sass-loader](/loaders/sass-loader)
//           { loader: "sass-loader" },
//         ],
//       },
//     ],
//   },

/*for package.json */
//  "scripts": {
//     "start": "node ./dist/bundle.js",
//     "watch": "webpack --watch",
//     "build": "webpack --mode=production",
//     "dev": "webpack serve --open",
//     "serve": "webpack serve --mode=production",
//     "test": "echo \"Error: no test specified\" && exit 1"
//   },
